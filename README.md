## JavaDevs App

This is a sample app showing java developers using Github REST api. It is written in kotlin language.

---

## Feature
This app contains below features.
1. Devs List with name and avatar
2. Detail view of developer

---

## Tech Stack
1. MVVM
2. Retrofit
3. Room
4. Coroutines
5. Unit Testing
6. Mockito
7. Dagger
8. Android Architecture Components