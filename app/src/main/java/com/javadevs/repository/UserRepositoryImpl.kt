package com.javadevs.repository

import com.javadevs.api.services.AppService
import com.javadevs.db.User
import com.javadevs.db.UserDao
import javax.inject.Inject

class UserRepositoryImpl @Inject constructor(
    private val userDao: UserDao,
    private val appService: AppService,
    private val mapper: UserResponseToDbMapper
) : UserRepository {

    /**
     * It will fetch users from api and then delete old entries and
     * insert into local db
     */
    override suspend fun getAllUsers(): List<User> {
        appService.getJavaUsers().let {
            return if (it.items.isNotEmpty()) {
                deleteAllUsers()
                insertUsersToDb(mapper.map(it.items))
                getAllUsersFromDb()
            } else {
                emptyList()
            }
        }
    }

    /**
     * it insert user list into local db
     */
    override suspend fun insertUsersToDb(users: List<User>) {
        userDao.insertUsers(users)
    }

    /**
     * it get all users from local db
     */
    override suspend fun getAllUsersFromDb(): List<User> {
        return userDao.getAllUsers()
    }

    /**
     * it get user by id from local db
     */
    override suspend fun getUserById(userId: Int): User? {
        return userDao.getUserDetailById(userId)
    }

    override suspend fun deleteAllUsers() {
        userDao.deleteAllUsers()
    }
}