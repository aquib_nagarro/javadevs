package com.javadevs.repository

import com.javadevs.db.User

interface UserRepository {
    suspend fun getAllUsers(): List<User>
    suspend fun insertUsersToDb(users: List<User>)
    suspend fun getAllUsersFromDb(): List<User>
    suspend fun getUserById(userId: Int): User?
    suspend fun deleteAllUsers()
}