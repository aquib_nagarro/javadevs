package com.javadevs.repository

import com.javadevs.api.responses.User
import com.javadevs.mapper.Mapper

class UserResponseToDbMapper : Mapper<List<User>, List<com.javadevs.db.User>> {

    /**
     * it maps api response list into local db user list
     */
    override suspend fun map(from: List<User>): List<com.javadevs.db.User> =
        from.map {
            com.javadevs.db.User(
                id = it.id,
                username = it.login,
                avatarUrl = it.avatarUrl,
                eventsUrl = it.eventsUrl,
                followersUrl = it.followersUrl,
                followingUrl = it.followingUrl,
                gistsUrl = it.gistsUrl,
                organizationsUrl = it.organizationsUrl,
                reposUrl = it.reposUrl,
                score = it.score,
                starredUrl = it.starredUrl,
                type = it.type,
                url = it.url,
                gravatarId = it.gravatarId
            )
        }
}