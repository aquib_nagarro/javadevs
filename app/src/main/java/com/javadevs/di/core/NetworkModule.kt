package com.javadevs.di.core

import android.content.Context
import com.javadevs.BuildConfig
import com.javadevs.api.services.AppService
import com.javadevs.db.UserDao
import com.javadevs.repository.UserRepository
import com.javadevs.repository.UserRepositoryImpl
import com.javadevs.repository.UserResponseToDbMapper
import com.javadevs.utils.NetworkConnectionInterceptor
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module(includes = [
    ContextModule::class,
    DatabaseModule::class,
    MapperModule::class])
class NetworkModule {

    @Singleton
    @Provides
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return httpLoggingInterceptor
    }

    @Singleton
    @Provides
    fun provideHttpClient(interceptor: HttpLoggingInterceptor,
                          context: Context): OkHttpClient {
        val clientBuilder = OkHttpClient.Builder()
        if (BuildConfig.DEBUG) {
            clientBuilder.addInterceptor(interceptor)
        }
        clientBuilder.addInterceptor(NetworkConnectionInterceptor(context))
        return clientBuilder.build()
    }

    @Singleton
    @Provides
    fun provideRetrofitBuilder(okHttpClient: OkHttpClient) =
        Retrofit.Builder()
            .baseUrl(BuildConfig.APP_API_BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(MoshiConverterFactory.create())
            .build()

    @Singleton
    @Provides
    fun provideAppService(retrofit: Retrofit) = retrofit.create(AppService::class.java)

    @Singleton
    @Provides
    fun provideUserRepository(userDao: UserDao, service: AppService,
                              mapper: UserResponseToDbMapper) : UserRepository =
        UserRepositoryImpl(userDao, service, mapper)
}