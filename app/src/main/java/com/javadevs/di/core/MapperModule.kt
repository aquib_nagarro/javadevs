package com.javadevs.di.core

import com.javadevs.repository.UserResponseToDbMapper
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class MapperModule {

    @Singleton
    @Provides
    fun providesMapper() = UserResponseToDbMapper()
}