package com.javadevs.di.core

import android.content.Context
import androidx.room.Room
import com.javadevs.BuildConfig
import com.javadevs.db.UserDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {

    @Singleton
    @Provides
    fun provideMarvelDatabase(context: Context) =
        Room.databaseBuilder(
            context,
            UserDatabase::class.java,
            BuildConfig.APP_DATABASE_NAME
        ).build()

    @Singleton
    @Provides
    fun provideUsersDao(userDatabase: UserDatabase) =
        userDatabase.userDao()
}