package com.javadevs.di.core

import android.content.Context
import com.javadevs.api.services.AppService
import com.javadevs.db.UserDao
import com.javadevs.repository.UserRepository
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [
    ContextModule::class,
    NetworkModule::class,
    DatabaseModule::class
])
interface CoreComponent {
    fun context(): Context
    fun appService(): AppService
    fun userRepository() : UserRepository
    fun usersDao(): UserDao
}