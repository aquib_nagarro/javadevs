package com.javadevs.di.app

import com.javadevs.SampleApp
import com.javadevs.di.core.CoreComponent
import com.javadevs.di.scopes.AppScope
import dagger.Component

@AppScope
@Component(
    dependencies = [CoreComponent::class],
    modules = [AppModule::class]
)
interface AppComponent {
    fun inject(application: SampleApp)
}