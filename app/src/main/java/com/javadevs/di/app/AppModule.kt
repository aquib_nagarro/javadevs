package com.javadevs.di.app

import android.content.Context
import com.javadevs.SampleApp
import dagger.Module
import dagger.Provides

@Module
class AppModule {
    @Provides
    fun provideContext(application: SampleApp): Context =
        application.applicationContext
}