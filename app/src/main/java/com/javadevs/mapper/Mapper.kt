package com.javadevs.mapper

interface Mapper<F, T> {
    suspend fun map(from: F): T
}