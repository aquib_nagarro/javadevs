package com.javadevs.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user")
data class User(
    @PrimaryKey val id: Int,
    @ColumnInfo(name = "username") val username: String,
    @ColumnInfo(name = "avatar_url") val avatarUrl: String,
    @ColumnInfo(name = "events_url") val eventsUrl: String,
    @ColumnInfo(name = "followers_url") val followersUrl: String,
    @ColumnInfo(name = "following_url") val followingUrl: String,
    @ColumnInfo(name = "gists_url") val gistsUrl: String,
    @ColumnInfo(name = "gravatar_id") val gravatarId: String,
    @ColumnInfo(name = "organizations_url") val organizationsUrl: String,
    @ColumnInfo(name = "repos_url") val reposUrl: String,
    @ColumnInfo(name = "score") val score: Int,
    @ColumnInfo(name = "starred_url") val starredUrl: String,
    @ColumnInfo(name = "type") val type: String,
    @ColumnInfo(name = "url") val url: String
)