package com.javadevs.db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface UserDao {

    @Query("SELECT * FROM user")
    suspend fun getAllUsers(): List<User>

    @Query(value = "SELECT * FROM user WHERE id = :userId")
    suspend fun getUserDetailById(userId: Int): User?

    @Insert
    suspend fun insertUsers(users: List<User>)

    @Insert
    suspend fun insertUser(user: User)

    @Query(value = "DELETE FROM user")
    suspend fun deleteAllUsers()
}