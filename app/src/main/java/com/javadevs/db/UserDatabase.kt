package com.javadevs.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.javadevs.BuildConfig

@Database(
    entities = [User::class],
    exportSchema = BuildConfig.APP_DATABASE_EXPORT_SCHEMA,
    version = BuildConfig.APP_DATABASE_VERSION
)
abstract class UserDatabase : RoomDatabase() {
    abstract fun userDao() : UserDao
}