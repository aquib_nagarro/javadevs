package com.javadevs.view.list

import com.javadevs.base.BaseViewState

sealed class UserListViewState : BaseViewState {
    object Loading : UserListViewState()
    object Error : UserListViewState()
    object Success : UserListViewState()
    object NetworkError : UserListViewState()

    fun isLoading() = this is Loading
    fun isSuccess() = this is Success
    fun isError() = this is Error
}