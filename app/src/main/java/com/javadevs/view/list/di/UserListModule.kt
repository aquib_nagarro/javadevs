package com.javadevs.view.list.di

import com.javadevs.di.scopes.FeatureScope
import com.javadevs.repository.UserRepository
import com.javadevs.utils.viewModel
import com.javadevs.view.list.UserListFragment
import com.javadevs.view.list.UserListViewModel
import com.javadevs.view.list.model.UserListDataMapper
import dagger.Module
import dagger.Provides

@Module
class UserListModule(
    val fragment: UserListFragment
){
    @FeatureScope
    @Provides
    fun provideUserListViewModel(
        userRepository: UserRepository,
        mapperList: UserListDataMapper
    ) = fragment.viewModel {
        UserListViewModel(
            userRepository = userRepository,
            userListDataMapper = mapperList)
    }

    @FeatureScope
    @Provides
    fun providesUserDataMapper() = UserListDataMapper()
}