package com.javadevs.view.list.di

import com.javadevs.di.core.CoreComponent
import com.javadevs.di.scopes.FeatureScope
import com.javadevs.view.list.UserListFragment
import dagger.Component

@FeatureScope
@Component(
    modules = [UserListModule::class],
    dependencies = [CoreComponent::class]
)
interface UserListComponent {
    fun inject(userListFragment: UserListFragment)
}