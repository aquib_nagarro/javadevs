package com.javadevs.view.list.model

data class User(
    val id: Int,
    val username: String,
    val avatarUrl: String,
    val eventsUrl: String,
    val followersUrl: String,
    val followingUrl: String,
    val gistsUrl: String,
    val gravatarId: String,
    val organizationsUrl: String,
    val reposUrl: String,
    val score: Int,
    val starredUrl: String,
    val type: String,
    val url: String
)