package com.javadevs.view.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.javadevs.repository.UserRepository
import com.javadevs.utils.NoConnectivityException
import com.javadevs.view.list.model.User
import com.javadevs.view.list.model.UserListDataMapper
import kotlinx.coroutines.launch
import javax.inject.Inject

class UserListViewModel
@Inject constructor(
    val userRepository: UserRepository,
    val userListDataMapper: UserListDataMapper
) : ViewModel() {
    private val _data = MutableLiveData<List<User>>()
    val data: LiveData<List<User>>
        get() = _data

    private val _state = MutableLiveData<UserListViewState>()
    val state: LiveData<UserListViewState>
        get() = _state

    /**
     * This method will load users from local database
     */
    fun loadUsers(){
        _state.postValue(UserListViewState.Loading)
        viewModelScope.launch {
            try {
                val result = userRepository.getAllUsers()
                _data.postValue(userListDataMapper.map(result))
                _state.postValue(UserListViewState.Success)
            } catch (e: Exception){
                if (e is NoConnectivityException){
                    _state.postValue(UserListViewState.NetworkError)
                } else {
                    _state.postValue(UserListViewState.Error)
                }
            }
        }
    }
}