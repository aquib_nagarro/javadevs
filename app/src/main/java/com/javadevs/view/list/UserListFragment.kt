package com.javadevs.view.list

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.firebase.analytics.FirebaseAnalytics
import com.javadevs.R
import com.javadevs.SampleApp
import com.javadevs.base.BaseFragment
import com.javadevs.utils.RecyclerViewItemDecoration
import com.javadevs.view.list.adapter.UserListAdapter
import com.javadevs.view.list.di.DaggerUserListComponent
import com.javadevs.view.list.di.UserListModule
import com.javadevs.view.list.model.User
import kotlinx.android.synthetic.main.fragment_user_list.*
import kotlinx.android.synthetic.main.view_user_list.*

class UserListFragment : BaseFragment<UserListViewModel>(
    layoutId = R.layout.fragment_user_list
), UserListAdapter.Interaction {
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private lateinit var userListAdapter: UserListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        firebaseAnalytics = FirebaseAnalytics.getInstance(context!!)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        swipe_refresh.setOnRefreshListener {
            loadUsers()
        }
        initRecyclerView()
        loadUsers()
    }

    /**
     * It will load users from local db
     */
    private fun loadUsers(){
        observeViewState()
        viewModel.loadUsers()
        trackLoadUsersEvent()
    }

    /**
     * It will track load users event
     */
    private fun trackLoadUsersEvent(){
        firebaseAnalytics.logEvent("load_users_event", null)
    }

    /**
     * Initialize recyclerview configuration
     */
    private fun initRecyclerView(){
        user_list.apply {
            user_list.addItemDecoration(RecyclerViewItemDecoration(resources.
            getDimension(R.dimen.user_list_item_padding).toInt()))
            userListAdapter = UserListAdapter(this@UserListFragment)
            user_list.adapter = userListAdapter
        }
    }

    /**
     * Initialize dependencies
     */
    override fun onInitDependencyInjection() {
        DaggerUserListComponent
            .builder()
            .coreComponent(SampleApp.coreComponent(requireContext()))
            .userListModule(UserListModule(this))
            .build()
            .inject(this)
    }

    private fun observeViewState(){
        swipe_refresh.isRefreshing = false
        viewModel.state.observe(this, Observer {
            when(it){
                is UserListViewState.Loading -> showLoading()
                is UserListViewState.Success -> {
                    hideLoading()
                    hideNetworkErrorView()
                    viewModel.data.observe(this, Observer {
                        userListAdapter.submitList(it)
                    })
                }
                is UserListViewState.NetworkError -> showNetworkErrorView()
                is UserListViewState.Error -> showEmptyList()
            }
        })
    }

    private fun showNetworkErrorView(){
        hideLoading()
        include_network_error_view.visibility = View.VISIBLE
    }

    private fun hideNetworkErrorView(){
        include_network_error_view.visibility = View.GONE
    }

    private fun showLoading(){
        loading.visibility = View.VISIBLE
    }

    private fun hideLoading(){
        loading.visibility = View.GONE
    }

    private fun showEmptyList(){
        hideLoading()
        include_list_empty.visibility = View.VISIBLE
    }

    /**
     * Callback method for user list item selection
     */
    override fun onItemSelected(position: Int, user: User) {
        val userId = user.id
        val action = UserListFragmentDirections.actionUserListFragmentToDetailFragment(userIdArg = userId)
        findNavController().navigate(action)
        trackUserItemClick(user = user)
    }

    /**
     * it will track user item click
     */
    private fun trackUserItemClick(user: User){
        val params = Bundle()
        params.putInt(FirebaseAnalytics.Param.ITEM_ID, user.id)
        params.putString(FirebaseAnalytics.Param.ITEM_NAME, user.username)
        firebaseAnalytics.logEvent("click_user", params)
    }
}