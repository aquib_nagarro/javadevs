package com.javadevs.view.list.model

import com.javadevs.db.User
import com.javadevs.mapper.Mapper

class UserListDataMapper : Mapper<List<User>, List<com.javadevs.view.list.model.User>> {
    @Throws(NoSuchElementException::class)
    override suspend fun map(users: List<User>): List<com.javadevs.view.list.model.User> {
        return users.map { user ->
            com.javadevs.view.list.model.User(
                id = user.id,
                username = user.username,
                avatarUrl = user.avatarUrl,
                eventsUrl = user.eventsUrl,
                followersUrl = user.followersUrl,
                followingUrl = user.followingUrl,
                gistsUrl = user.gistsUrl,
                gravatarId = user.gravatarId,
                organizationsUrl = user.organizationsUrl,
                reposUrl = user.reposUrl,
                score = user.score,
                starredUrl = user.starredUrl,
                type = user.type,
                url = user.url
            )
        }
    }
}
