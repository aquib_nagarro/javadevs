package com.javadevs.view.detail.di

import com.javadevs.di.scopes.FeatureScope
import com.javadevs.repository.UserRepository
import com.javadevs.utils.viewModel
import com.javadevs.view.detail.UserDetailFragment
import com.javadevs.view.detail.UserDetailViewModel
import com.javadevs.view.detail.mapper.UserDetailMapper
import dagger.Module
import dagger.Provides

@Module
class UserDetailModule(
    val fragment: UserDetailFragment
) {
    @FeatureScope
    @Provides
    fun provideUserDetailViewModel(
        userRepository: UserRepository,
        detailMapper: UserDetailMapper
    ) = fragment.viewModel {
        UserDetailViewModel(
            userRepository = userRepository,
            userDetailMapper = detailMapper)
    }

    @FeatureScope
    @Provides
    fun providesUserDetailMapper() = UserDetailMapper()
}