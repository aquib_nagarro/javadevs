package com.javadevs.view.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.javadevs.repository.UserRepository
import com.javadevs.view.detail.mapper.UserDetailMapper
import com.javadevs.view.list.model.User
import kotlinx.coroutines.launch
import javax.inject.Inject

class UserDetailViewModel
@Inject constructor(
    val userRepository: UserRepository,
    val userDetailMapper: UserDetailMapper
) : ViewModel() {
    private val _data = MutableLiveData<User>()
    val data: LiveData<User>
        get() = _data

    private val _state = MutableLiveData<UserDetailViewState>()
    val state: LiveData<UserDetailViewState>
        get() = _state

    /**
     * This method will fetch user from local db using id
     */
    fun loadUserById(userId: Int){
        viewModelScope.launch {
            try {
                val result = userRepository.getUserById(userId = userId)
                _data.postValue(result?.let { userDetailMapper.map(it) })
                _state.postValue(UserDetailViewState.Success)
            } catch (e: Exception){
                _state.postValue(UserDetailViewState.Error)
            }
        }
    }
}