package com.javadevs.view.detail.di

import com.javadevs.di.core.CoreComponent
import com.javadevs.di.scopes.FeatureScope
import com.javadevs.view.detail.UserDetailFragment
import dagger.Component

@FeatureScope
@Component(
    modules = [UserDetailModule::class],
    dependencies = [CoreComponent::class]
)
interface UserDetailComponent {
    fun inject(userDetailFragment: UserDetailFragment)
}