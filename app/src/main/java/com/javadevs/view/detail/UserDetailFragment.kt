package com.javadevs.view.detail

import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import coil.api.load
import com.google.firebase.analytics.FirebaseAnalytics
import com.javadevs.R
import com.javadevs.SampleApp
import com.javadevs.base.BaseFragment
import com.javadevs.view.detail.di.DaggerUserDetailComponent
import com.javadevs.view.detail.di.UserDetailModule
import com.javadevs.view.list.model.User
import kotlinx.android.synthetic.main.fragment_detail.*

class UserDetailFragment : BaseFragment<UserDetailViewModel>(
    layoutId = R.layout.fragment_detail
) {
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private val args: UserDetailFragmentArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        firebaseAnalytics = FirebaseAnalytics.getInstance(context!!)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val userId = args.userIdArg
        observeViewState()
        loadUserById(userId)
    }

    override fun onResume() {
        super.onResume()
        firebaseAnalytics.logEvent("user_detail_page", null)
    }

    /**
     * This method will load user from local db using id
     */
    private fun loadUserById(userId: Int){
        viewModel.loadUserById(userId = userId)
        trackLoadUserById()
    }

    /**
     * This method will track load user by id event
     */
    private fun trackLoadUserById(){
        firebaseAnalytics.logEvent("load_user_by_id_event", null)
    }

    /**
     * It will initialize dependency
     */
    override fun onInitDependencyInjection() {
        DaggerUserDetailComponent
            .builder()
            .coreComponent(SampleApp.coreComponent(requireContext()))
            .userDetailModule(UserDetailModule(this))
            .build()
            .inject(this)
    }

    /**
     * It will observe user detail view state
     */
    private fun observeViewState(){
        viewModel.state.observe(this, Observer {
            when(it){
                is UserDetailViewState.Success -> {
                    viewModel.data.observe(this, Observer {
                        bindUserToView(it)
                    })
                }
                is UserDetailViewState.Error -> Toast.makeText(context,
                    "Invalid User", Toast.LENGTH_SHORT).show()
            }
        })
    }

    /**
     * It will bind user data to view
     */
    private fun bindUserToView(user: User){
        user_image.load(user.avatarUrl)
        user_name.text = user.username
        user_profile_link.text = user.url
        user_profile_link.movementMethod = LinkMovementMethod.getInstance()
    }
}