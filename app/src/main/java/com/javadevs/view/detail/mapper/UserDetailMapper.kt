package com.javadevs.view.detail.mapper

import com.javadevs.db.User
import com.javadevs.mapper.Mapper

class UserDetailMapper : Mapper<User, com.javadevs.view.list.model.User>{
    override suspend fun map(user: User): com.javadevs.view.list.model.User {
        return com.javadevs.view.list.model.User(
            id = user.id,
            username = user.username,
            avatarUrl = user.avatarUrl,
            eventsUrl = user.eventsUrl,
            followersUrl = user.followersUrl,
            followingUrl = user.followingUrl,
            gistsUrl = user.gistsUrl,
            gravatarId = user.gravatarId,
            organizationsUrl = user.organizationsUrl,
            reposUrl = user.reposUrl,
            score = user.score,
            starredUrl = user.starredUrl,
            type = user.type,
            url = user.url
        )
    }
}