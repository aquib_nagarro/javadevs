package com.javadevs.view.detail

import com.javadevs.base.BaseViewState

sealed class UserDetailViewState : BaseViewState {
    object Error : UserDetailViewState()
    object Success : UserDetailViewState()

    fun isSuccess() = this is Success
    fun isError() = this is Error
}