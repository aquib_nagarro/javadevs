package com.javadevs

import android.app.Application
import android.content.Context
import androidx.appcompat.app.AppCompatDelegate
import com.javadevs.di.app.DaggerAppComponent
import com.javadevs.di.core.ContextModule
import com.javadevs.di.core.CoreComponent
import com.javadevs.di.core.DaggerCoreComponent

class SampleApp : Application() {
    lateinit var coreComponent: CoreComponent

    companion object {
        fun coreComponent(context: Context) =
            (context.applicationContext as? SampleApp)?.coreComponent
    }

    override fun onCreate() {
        super.onCreate()
        initCoreDependencyInjection()
        initAppDependencyInjection()
        initNightMode()
    }

    private fun initCoreDependencyInjection(){
        coreComponent = DaggerCoreComponent
            .builder()
            .contextModule(ContextModule(this))
            .build()
    }

    private fun initAppDependencyInjection(){
        DaggerAppComponent
            .builder()
            .coreComponent(coreComponent)
            .build()
            .inject(this)
    }

    /**
     * force initialize night mode
     */
    private fun initNightMode() {
        if (BuildConfig.DEBUG) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        }
    }
}