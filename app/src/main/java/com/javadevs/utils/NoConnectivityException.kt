package com.javadevs.utils

import java.io.IOException

class NoConnectivityException : IOException() {

    override fun getLocalizedMessage(): String? {
        return "No Internet Connection"
    }
}