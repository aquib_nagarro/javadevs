package com.javadevs.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.NetworkInfo
import android.os.Build
import okhttp3.Interceptor
import okhttp3.Response

class NetworkConnectionInterceptor(
    private val context: Context
) : Interceptor {

    /**
     * Interceptor for checking network connectivity
     */
    override fun intercept(chain: Interceptor.Chain): Response {
        if(!isConnected()){
            throw NoConnectivityException()
        }

        val builder = chain.request().newBuilder()
        return chain.proceed(builder.build())
    }

    /**
     * It will verify internet connectivity
     */
    private fun isConnected() : Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE)
                as ConnectivityManager
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            val activeNetwork = connectivityManager.activeNetwork ?: return false
            val networkCap : NetworkCapabilities = connectivityManager.getNetworkCapabilities(activeNetwork)!!
            when {
                networkCap.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                networkCap.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                networkCap.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                else -> false
            }
        } else {
            val nwInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
            nwInfo?.isConnected!!
        }
    }
}