package com.javadevs.api.responses

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class User(
    @field:Json(name = "id") val id: Int,
    @field:Json(name = "login") val login: String,
    @field:Json(name = "avatar_url") val avatarUrl: String,
    @field:Json(name = "events_url") val eventsUrl: String,
    @field:Json(name = "followers_url") val followersUrl: String,
    @field:Json(name = "following_url") val followingUrl: String,
    @field:Json(name = "gists_url") val gistsUrl: String,
    @field:Json(name = "gravatar_id") val gravatarId: String,
    @field:Json(name = "organizations_url") val organizationsUrl: String,
    @field:Json(name = "repos_url") val reposUrl: String,
    @field:Json(name = "score") val score: Int,
    @field:Json(name = "starred_url") val starredUrl: String,
    @field:Json(name = "type") val type: String,
    @field:Json(name = "url") val url: String
)