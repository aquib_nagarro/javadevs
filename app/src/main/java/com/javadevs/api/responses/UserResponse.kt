package com.javadevs.api.responses

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class UserResponse(
    @field:Json(name = "total_count") val total: Int,
    @field:Json(name = "incomplete_results") val isIncompleteResult: Boolean = false,
    @field:Json(name = "items") val items: List<User>
)