package com.javadevs.api.services

import com.javadevs.api.responses.User
import com.javadevs.api.responses.UserResponse
import retrofit2.http.GET

interface AppService {

    @GET("/search/users?q=language:java+location:lagos")
    suspend fun getJavaUsers(): UserResponse
}