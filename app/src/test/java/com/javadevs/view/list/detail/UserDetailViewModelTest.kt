package com.javadevs.view.list.detail

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.javadevs.CoroutineRule
import com.javadevs.repository.UserRepository
import com.javadevs.view.detail.UserDetailViewModel
import com.javadevs.view.detail.UserDetailViewState
import com.javadevs.view.detail.mapper.UserDetailMapper
import com.javadevs.view.list.model.User
import io.mockk.*
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class UserDetailViewModelTest {
    @ExperimentalCoroutinesApi
    @get:Rule
    val coroutineTestRule = CoroutineRule()

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @MockK(relaxed = true)
    lateinit var userRepository: UserRepository
    @MockK(relaxed = true)
    lateinit var userDetailMapper: UserDetailMapper
    @MockK(relaxed = true)
    lateinit var stateObserver: Observer<UserDetailViewState>
    @MockK(relaxed = true)
    lateinit var dataObserver: Observer<User>
    lateinit var viewModel: UserDetailViewModel

    @Before
    fun setUp(){
        MockKAnnotations.init(this)
        viewModel = UserDetailViewModel(
            userRepository = userRepository,
            userDetailMapper = userDetailMapper
        )
        viewModel.state.observeForever(stateObserver)
        viewModel.data.observeForever(dataObserver)
    }


    @Test
    fun loadUserDetail_WhenError_ShouldBeErrorState() {
        viewModel.loadUserById(userId = 1234)

        val expectedState: UserDetailViewState = UserDetailViewState.Error
        Assert.assertEquals(expectedState, viewModel.state.value)
        verify { stateObserver.onChanged(expectedState) }
    }

    @Test
    fun loadUserDetail_WhenSuccess_ShouldPostDataResult() {
        val userId = 1234
        val user = mockk<User>()
        val userDb = mockk<com.javadevs.db.User>()
        coEvery { userRepository.getUserById(userId) } returns userDb
        coEvery { userDetailMapper.map(any()) } returns user

        viewModel.loadUserById(userId)

        verify { dataObserver.onChanged(user) }
        coVerify { userRepository.getUserById(userId) }
        coVerify { userDetailMapper.map(userDb) }
    }
}