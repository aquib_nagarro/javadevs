package com.javadevs.view.list

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.javadevs.CoroutineRule
import com.javadevs.api.responses.UserResponse
import com.javadevs.repository.UserRepository
import com.javadevs.view.list.model.User
import com.javadevs.view.list.model.UserListDataMapper
import io.mockk.*
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock

class UserListViewModelTest {
    @ExperimentalCoroutinesApi
    @get:Rule
    val coroutineTestRule = CoroutineRule()

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @MockK(relaxed = true)
    lateinit var userRepository: UserRepository
    @MockK(relaxed = true)
    lateinit var userListDataMapper: UserListDataMapper
    @MockK(relaxed = true)
    lateinit var stateObserver: Observer<UserListViewState>
    @MockK(relaxed = true)
    lateinit var dataObserver: Observer<List<User>>
    lateinit var viewModel: UserListViewModel

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        viewModel = UserListViewModel(
            userRepository = userRepository,
            userListDataMapper = userListDataMapper
        )
        viewModel.state.observeForever(stateObserver)
        viewModel.data.observeForever(dataObserver)
    }

    @Test
    fun loadUserList_ShouldSetLoadingState() {
        viewModel.loadUsers()
        verify { stateObserver.onChanged(UserListViewState.Loading) }
    }

    @Test
    fun loadUserList_WhenError_ShouldBeErrorState() {
        viewModel.loadUsers()

        val expectedState: UserListViewState = UserListViewState.Error
        Assert.assertEquals(expectedState, viewModel.state.value)
        verify { stateObserver.onChanged(expectedState) }
    }

    @Test
    fun loadUserList_WhenSuccess_ShouldPostDataResult() {
        val userList = mockk<List<User>>()
        val userListDb = mockk<List<com.javadevs.db.User>>()
        coEvery { userRepository.getAllUsers() } returns userListDb
        coEvery { userListDataMapper.map(any()) } returns userList

        viewModel.loadUsers()

        verify { dataObserver.onChanged(userList) }
        coVerify { userRepository.getAllUsers() }
        coVerify { userListDataMapper.map(userListDb) }
    }
}

