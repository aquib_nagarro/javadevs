package com.javadevs.db

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import kotlinx.coroutines.runBlocking
import okio.IOException
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class UserDaoTest {
    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var userDao: UserDao
    private lateinit var userDatabase: UserDatabase
    private val fakeUsers = listOf(
        User(8110201, username = "moyheen", avatarUrl = "https://avatars2.githubusercontent.com/u/8110201?v=4",
        eventsUrl = "https://api.github.com/users/moyheen/events%7B/privacy%7D",
        followersUrl = "https://api.github.com/users/moyheen/events%7B/privacy%7D",
        followingUrl = "https://api.github.com/users/moyheen/events%7B/privacy%7D",
        gistsUrl = "https://api.github.com/users/moyheen/events%7B/privacy%7D",
        gravatarId = "fafgss",
        organizationsUrl = "https://api.github.com/users/moyheen/events%7B/privacy%7D",
        reposUrl = "https://api.github.com/users/moyheen/events%7B/privacy%7D",
        score = 123,
        starredUrl = "https://api.github.com/users/moyheen/events%7B/privacy%7D",
        type = "User",
        url = "https://api.github.com/users/moyheen"),

        User(25801929, username = "chalu", avatarUrl = "https://avatars2.githubusercontent.com/u/8110201?v=4",
            eventsUrl = "https://api.github.com/users/moyheen/events%7B/privacy%7D",
            followersUrl = "https://api.github.com/users/moyheen/events%7B/privacy%7D",
            followingUrl = "https://api.github.com/users/moyheen/events%7B/privacy%7D",
            gistsUrl = "https://api.github.com/users/moyheen/events%7B/privacy%7D",
            gravatarId = "fafgss",
            organizationsUrl = "https://api.github.com/users/moyheen/events%7B/privacy%7D",
            reposUrl = "https://api.github.com/users/moyheen/events%7B/privacy%7D",
            score = 123,
            starredUrl = "https://api.github.com/users/moyheen/events%7B/privacy%7D",
            type = "User",
            url = "https://api.github.com/users/moyheen"),

        User(276286, username = "aliumujib", avatarUrl = "https://avatars2.githubusercontent.com/u/8110201?v=4",
            eventsUrl = "https://api.github.com/users/moyheen/events%7B/privacy%7D",
            followersUrl = "https://api.github.com/users/moyheen/events%7B/privacy%7D",
            followingUrl = "https://api.github.com/users/moyheen/events%7B/privacy%7D",
            gistsUrl = "https://api.github.com/users/moyheen/events%7B/privacy%7D",
            gravatarId = "fafgss",
            organizationsUrl = "https://api.github.com/users/moyheen/events%7B/privacy%7D",
            reposUrl = "https://api.github.com/users/moyheen/events%7B/privacy%7D",
            score = 123,
            starredUrl = "https://api.github.com/users/moyheen/events%7B/privacy%7D",
            type = "User",
            url = "https://api.github.com/users/moyheen")
    )

    @Before
    fun createDb() = runBlocking{
        val context = ApplicationProvider.getApplicationContext<Context>()
        userDatabase = Room
            .inMemoryDatabaseBuilder(context, UserDatabase::class.java)
            .allowMainThreadQueries()
            .build()
        userDao = userDatabase.userDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb(){
        userDatabase.close()
    }

    @Test
    fun getAllUsers_WithoutData_ShouldReturnNull() = runBlocking{
        val users = userDao.getAllUsers()
        assertTrue(users.isNullOrEmpty())
    }

    @Test
    fun getAllUsers_WithData_ShouldReturnSorted() = runBlocking {
        userDao.insertUsers(fakeUsers)
        assertEquals(fakeUsers, userDao.getAllUsers().sortedByDescending { it.username })
    }

    @Test
    fun getUserById_WithoutData_ShouldNotFound() = runBlocking {
        val user = fakeUsers.first()
        assertNull(userDao.getUserDetailById(user.id))
    }

    @Test
    fun getUserById_WithData_ShouldFound() = runBlocking {
        userDao.insertUsers(fakeUsers)
        val user = fakeUsers.first()
        assertEquals(user, userDao.getUserDetailById(userId = user.id))
    }

    @Test
    fun insertUser_GetUserById_ShouldFound() = runBlocking {
        val user = getFakeUser()
        userDao.insertUser(user)
        assertEquals(user, userDao.getUserDetailById(userId = user.id))
    }

    @Test
    fun insertUser_GetUserById_ShouldNotFound() = runBlocking {
        val user = getFakeUser()
        userDao.insertUser(user)
        assertNotEquals(user, userDao.getUserDetailById(userId = 12345))
    }

    @Test
    fun deleteAllUsers_ShouldRemoveAll() = runBlocking {
        userDao.insertUsers(fakeUsers)
        userDao.deleteAllUsers()

        assertTrue(userDao.getAllUsers().isNullOrEmpty())
    }

    fun getFakeUser(): User {
        return User(8110201, username = "moyheen", avatarUrl = "https://avatars2.githubusercontent.com/u/8110201?v=4",
            eventsUrl = "https://api.github.com/users/moyheen/events%7B/privacy%7D",
            followersUrl = "https://api.github.com/users/moyheen/events%7B/privacy%7D",
            followingUrl = "https://api.github.com/users/moyheen/events%7B/privacy%7D",
            gistsUrl = "https://api.github.com/users/moyheen/events%7B/privacy%7D",
            gravatarId = "fafgss",
            organizationsUrl = "https://api.github.com/users/moyheen/events%7B/privacy%7D",
            reposUrl = "https://api.github.com/users/moyheen/events%7B/privacy%7D",
            score = 123,
            starredUrl = "https://api.github.com/users/moyheen/events%7B/privacy%7D",
            type = "User",
            url = "https://api.github.com/users/moyheen")
    }
}